package fr.but3;

import java.util.Date;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        CarteEmbarquement carteEmbarquement = new CarteEmbarquement("MATHIEU", "AF443", "A52", "10:35");
        ServiceEnregistrement serviceEnregistrement = new ServiceEnregistrement();

        String json = serviceEnregistrement.encodeCarte(carteEmbarquement);
        String checksum = serviceEnregistrement.calculerEmpreinte(json);

        System.out.println("json: "+json);
        System.out.println("checksum: " +checksum);
        System.out.println(serviceEnregistrement.lireEtVerifier(json, checksum));

        // Alter json
        json = json.substring(0, 10) + '0' + json.substring(11);

        System.out.println(serviceEnregistrement.lireEtVerifier(json, checksum));
    }

    public record CarteEmbarquement(String nomPassager, String numeroVol, String porteEmbarquement, String heureDecollage) { }

}
