package fr.but3;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.but3.App.CarteEmbarquement;

public class ServiceEnregistrement {

    private ObjectMapper objectMapper;

    public ServiceEnregistrement() {
        this.objectMapper = new ObjectMapper();
    }

    public String encodeCarte(CarteEmbarquement carteEmbarquement) {
        try {
            return this.objectMapper.writeValueAsString(carteEmbarquement);
        }catch(Exception e) {
            System.err.println(e);
            return null;
        }
    }

    public String calculerEmpreinte(final String string) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");

            byte[] hash = messageDigest.digest(string.getBytes(StandardCharsets.UTF_8));

            return Base64.getEncoder().encodeToString(hash);
            //return calculatedBytes.toString();
        } catch (NoSuchAlgorithmException e) {
            System.err.println(e);
            return null;
        }

    }

    public CarteEmbarquement lireEtVerifier(String json, String checksum) {
        try {
            if( this.calculerEmpreinte(json).equals(checksum)) {
                return this.objectMapper.readValue(json, CarteEmbarquement.class);
            } else {
                throw new IllegalArgumentException();
            }
        }catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

}
